#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>

void * ping(void *a);
void * pong(void *a);
float timedifference_msec(struct timeval t0, struct timeval t1);

pthread_mutex_t MTX = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t COND = PTHREAD_COND_INITIALIZER;

int ping_turn = 1;

int main(int argc, char **argv)
{
  if (argc < 2){
    printf("Musisz podać jakiś numer.\n" );
    return 0;
  }
  int n = atoi(argv[1]);
  int result;

  struct timeval stop, start;
  gettimeofday(&start, NULL);

  pthread_t ping_tid;
  result = pthread_create(&ping_tid, 0, ping, &n);
  if (result){
    perror("pthread_create ping: ");
    exit(1);
  }

  pthread_t pong_tid;
  result = pthread_create(&pong_tid, 0, pong, &n);
  if (result){
    perror("pthread_create pong: ");
    exit(1);
  }

  result = pthread_join(pong_tid, 0);
  if (result){
    perror("pthread_join pong: ");
    exit(1);
  }

  result = pthread_join(ping_tid, 0);
  if (result){
    perror("pthread_join ping: ");
    exit(1);
  }

  gettimeofday(&stop, NULL);
  float elapsed = timedifference_msec(start, stop);
  int seconds = elapsed / 1000.0f;
  float mili = elapsed - (float)seconds;
  int rest = (int) mili / 100;
  printf("Program wykonywał się %d.%d\n", seconds, rest) ;
  return 0;
}

void * ping(void *a)
{
  int n = *(int *) a;
  int i;

  for (i = 0; i < n; i++){
    pthread_mutex_lock(&MTX);
    while (ping_turn == 0){
      pthread_cond_wait(&COND, &MTX);
    }
    printf("PING ");
    ping_turn = 0;
    pthread_cond_signal(&COND);
    pthread_mutex_unlock(&MTX);
  }
  return 0;
}

void * pong(void *a)
{
  int n = *(int *) a;
  int i;

  for (i = 0; i < n; i++){
    pthread_mutex_lock(&MTX);
    while (ping_turn == 1){
      pthread_cond_wait(&COND, &MTX);
    }
    printf("pong\n");
    ping_turn = 1;
    pthread_cond_signal(&COND);
    pthread_mutex_unlock(&MTX);
  }
  return 0;
}
float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}
