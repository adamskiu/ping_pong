TARGET = pong
CC = gcc
CFLAGS = -Wall -Werror -g
LDFLAGS = -pthread 
SOURCES = main.c

all : 
	$(CC) $(SOURCES) $(CFLAGS) $(LDFLAGS) -o $(TARGET)

clean:
	rm -f $(TARGET)
